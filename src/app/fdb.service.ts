import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { FirebaseFirestore } from 'angularfire2';
import { Observable } from 'rxjs';

export interface BasicData { email: string; cp: number; anyo: number; direccion: string; reforma: string; dateSaved: number }

@Injectable()
export class FdbService {
  private bdCollection: AngularFirestoreCollection<BasicData>;
  private bdCollectionGeneric: AngularFirestoreCollection<{}>;
  private bdCollectionCounts: AngularFirestoreCollection<{}>;
  public bdBasicDataDocument: AngularFirestoreDocument<BasicData>;
  public bdBasicDataObservable: Observable<BasicData>;
  public basicData: BasicData;


  constructor(private afs: AngularFirestore) {
    this.bdCollection = afs.collection<BasicData>('basicData');
    this.bdCollectionCounts = afs.collection<BasicData>('count');
  }

  saveBasicData(id, email, cp, anyo, direccion, reforma) {
    const ts = Date.now();
    const item: BasicData = { email: email, cp: cp, anyo: anyo, direccion: direccion, reforma: reforma, dateSaved: ts };
    console.log ('Saving data');
    console.log (item);
    this.bdCollection.doc(id).set(item);
  }
  getBasicData(id) {
    this.bdBasicDataDocument = this.bdCollection.doc(id)
    this.bdBasicDataObservable = this.bdBasicDataDocument.valueChanges()
  }

  saveData (section, id, items) {
    this.bdCollectionGeneric = this.afs.collection<{}>(section);
    this.bdCollectionGeneric.doc(id).set(items);
    const c = { }
    c[id] = Date.now();
    this.bdCollectionCounts.doc(section).update(c);
  }

  getObservable (section, id) {
    this.bdCollectionGeneric = this.afs.collection<{}>(section);
    const bdDocument = this.bdCollectionGeneric.doc(id);
    return bdDocument.valueChanges();
  }

}
