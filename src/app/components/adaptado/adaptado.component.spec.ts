import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdaptadoComponent } from './adaptado.component';

describe('AdaptadoComponent', () => {
  let component: AdaptadoComponent;
  let fixture: ComponentFixture<AdaptadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdaptadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdaptadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
