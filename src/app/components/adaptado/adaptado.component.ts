import { Component, OnInit } from '@angular/core';
import { findLocaleData } from '@angular/common/src/i18n/locale_data_api';
import { GlobalService } from '../../global.service';
import * as jsPDF from 'jspdf';
import { FdbService } from 'app/fdb.service';
import { FauthService } from 'app/fauth.service';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-adaptado',
  templateUrl: './adaptado.component.html',
  styleUrls: ['./adaptado.component.scss']
})
export class AdaptadoComponent implements OnInit {

  dataImgLogos;
  dataImgDI;
  feedback = [];
  feedbackClean = [];
  feedDucha = [];
  feedEscalones = [];
  feedbackFijoComun = [];
  feedbackFijoVivienda = [];
  items = [];
  input = {};
  public bdDataObservable: Observable<{}>;
  private user: Observable<User>;
  showRecomendaciones = false;

  constructor(public globalService: GlobalService, public fdb: FdbService, public fauth: FauthService, translate: TranslateService) {
    this.user = fauth.afAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.getFromFirestoreChanges();
        }
      }
    );
    translate.get('DI-FIJOCOMUN').subscribe((res) => {
      this.feedbackFijoComun = res;
    });
    translate.get('DI-FIJOVIVIENDA').subscribe((res) => {
      this.feedbackFijoVivienda = res;
    });
    translate.get('DI-ITEMS').subscribe((res) => {
      this.items = res;
    });
    translate.get('DI-ZONADUCHA').subscribe((res) => {
      this.feedDucha = res;
    });
    translate.get('DI-ESCALONES').subscribe((res) => {
      this.feedEscalones = res;
    });

    translate.onLangChange.subscribe((event) => {
      translate.get('DI-FIJOCOMUN').subscribe((res) => {
        this.feedbackFijoComun = res;
      });
      translate.get('DI-FIJOVIVIENDA').subscribe((res) => {
        this.feedbackFijoVivienda = res;
      });
      translate.get('DI-ITEMS').subscribe((res) => {
        this.items = res;
      });
      translate.get('DI-ZONADUCHA').subscribe((res) => {
        this.feedDucha = res;
      });
      translate.get('DI-ESCALONES').subscribe((res) => {
        this.feedEscalones = res;
      });
    });
  }

  ngOnInit() {
  }

  onSelectionChange(i, op, feed, resp) {
    this.feedback[i] = feed;
    this.items[i].input = op;
    this.input[i] = resp;
    if (this.fauth.userDetails != null) {
      this.fdb.saveData('di', this.fauth.userDetails.uid, this.input);
    }
    // Opciones marcadas en Rojo en Feedback
    if (this.items[23].input === 1 && this.items[24].input === 1) {
      this.feedback[22] = this.feedDucha[0];
    } else if (this.items[23].input === 1 && this.items[24].input === 0) {
      this.feedback[22] = this.feedDucha[1];
    } else if (this.items[23].input === 0 && this.items[24].input === 1) {
      this.feedback[22] = this.feedDucha[2];
    } else if (this.items[23].input === 0 && this.items[24].input === 0) {
      this.feedback[22] = this.feedDucha[3];
    }

    if (i === 0) { // intervencion
      this.items[1].visible = this.items[0].input === 1 ? true : false;
      this.items[2].visible = this.items[0].input === 1 ? true : false;
      this.items[3].visible = this.items[0].input === 1 ? true : false;
      this.items[4].visible = this.items[0].input === 1 ? true : false;
      this.items[5].visible = this.items[0].input === 1 ? true : false;
      this.items[6].visible = this.items[0].input === 1 ? true : false;
      this.items[7].visible = this.items[0].input === 1 ? true : false;
      this.items[8].visible = this.items[0].input === 1 ? true : false;
    }
    if (i === 1) { // escalones
      this.items[2].visible = this.items[1].input === 1 ? false : true;
    }
    if (i === 3) { // ascensor
      this.items[4].visible = this.items[3].input === 1 ? false : true;
      this.items[5].visible = this.items[3].input === 1 ? false : true;
    }
    if (i === 6) { // rampas
      this.items[7].visible = this.items[6].input === 1 ? false : true;
      this.items[8].visible = this.items[6].input === 1 ? false : true;
    }
    if (i === 11) { // zonas comunes interior
      this.items[12].visible = this.items[11].input === 1 ? false : true;
    }
    if (i === 13) { // zonas comunes exterior
      this.items[14].visible = this.items[13].input === 1 ? false : true;
    }
    if (i === 15) { // aparcamiento
      this.items[16].visible = this.items[15].input === 1 ? false : true;
      this.items[17].visible = this.items[15].input === 1 ? false : true;
    }
    // escalones
    if (parseInt(this.items[2].input, 10) === 0) {
      this.feedback[2] = '';
    } else if (this.items[1].input === 0 && (parseInt(this.items[2].input, 10) < 4)) {
      this.feedback[2] = this.feedEscalones[0]
    } else if (this.items[1].input === 0 && (parseInt(this.items[2].input, 10) > 3)) {
      this.feedback[2] = this.feedEscalones[1]
    }
    this.feedbackClean = this.globalService.cleanFeedback(this.feedback);
  }

  getFromFirestoreChanges() {
    if (this.fauth.userDetails != null) {
      this.bdDataObservable = this.fdb.getObservable('di', this.fauth.userDetails.uid);
      this.bdDataObservable.subscribe(input => {
        if (input != null) {
          this.input = input;
          this.updateInput();
        }
      });
    }
  }


  updateInput() {
    this.feedback = [];
    // tslint:disable-next-line:forin
    for (const k in this.input) {
      if (this.items[k].opciones.length > 0) {
        this.feedback[k] = this.items[k].opciones[this.input[k]].feed;
      }
      this.items[k].input = this.input[k] ; // === 0 ? 0 : 1;
    }
    // Opciones marcadas en Rojo en Feedback
    if (this.items[23].input === 1 && this.items[24].input === 1) {
      this.feedback[22] = this.feedDucha[0];
    } else if (this.items[23].input === 1 && this.items[24].input === 0) {
      this.feedback[22] = this.feedDucha[1];
    } else if (this.items[23].input === 0 && this.items[24].input === 1) {
      this.feedback[22] = this.feedDucha[2];
    } else if (this.items[23].input === 0 && this.items[24].input === 0) {
      this.feedback[22] = this.feedDucha[3];
    }

    this.items[1].visible = this.items[0].input === 1 ? true : false;
    this.items[2].visible = this.items[0].input === 1 ? true : false;
    this.items[3].visible = this.items[0].input === 1 ? true : false;
    this.items[4].visible = this.items[0].input === 1 ? true : false;
    this.items[5].visible = this.items[0].input === 1 ? true : false;
    this.items[6].visible = this.items[0].input === 1 ? true : false;
    this.items[7].visible = this.items[0].input === 1 ? true : false;
    this.items[8].visible = this.items[0].input === 1 ? true : false;
    this.items[2].visible = this.items[1].input === 1 ? false : true;
    this.items[4].visible = this.items[3].input === 1 ? false : true;
    this.items[5].visible = this.items[3].input === 1 ? false : true;
    this.items[7].visible = this.items[6].input === 1 ? false : true;
    this.items[8].visible = this.items[6].input === 1 ? false : true;
    this.items[12].visible = this.items[11].input === 1 ? false : true;
    this.items[14].visible = this.items[13].input === 1 ? false : true;
    this.items[16].visible = this.items[15].input === 1 ? false : true;
    this.items[17].visible = this.items[15].input === 1 ? false : true;

    if (parseInt(this.items[2].input, 10) === 0) {
      this.feedback[2] = '';
    } else if (this.items[1].input === 0 && (parseInt(this.items[2].input, 10) < 4)) {
      this.feedback[2] = this.feedEscalones[0]
    } else if (this.items[1].input === 0 && (parseInt(this.items[2].input, 10) > 3)) {
      this.feedback[2] = this.feedEscalones[1]
    }
    this.feedbackClean = this.globalService.cleanFeedback(this.feedback);
  }

  generatePDF() {
    if (this.fauth.userDetails != null) {
      this.showRecomendaciones = true;
      // cabecera , imágenes
      const doc = new jsPDF({ lineHeight: 1.5 })
      doc.addImage(this.globalService.imgData.logos, 'PNG', 15, 15, 180, 33)
      doc.addImage(this.globalService.imgData.di, 'JPEG', 20, 50, 60, 40)
      doc.link(15, 15, 180, 33, { url: 'https://bes-viviendaturistica.com' });
      doc.setFontSize(16)
      doc.setFontType('bold')
      doc.setTextColor('#40694d')
      // titulo sección
      doc.text(90, 60, this.globalService.pdfTxt[0])
      doc.text(90, 70, this.globalService.pdfTxt[3])
      doc.text(90, 80, this.globalService.pdfTxt[1])
      let feedbackText = ''
      for (let i = 0; i < this.feedbackClean.length; i++) {
        feedbackText += this.feedbackClean[i] + '\n'
      }
      const lines = doc.setFontSize(10).splitTextToSize(feedbackText, 170)
      // fecha
      const d = new Date();
      doc.text(90, 90, d.toLocaleDateString('es-ES'));
      // datos básicos
      doc.text(20, 100, this.globalService.pdfTxt[7] + this.globalService.toStringAddress())
      doc.text(20, 105, this.globalService.pdfTxt[8] + this.globalService.year + '    ' + this.globalService.toStringReforma())
      doc.setFontType('normal')
      // lineas feedback
      doc.text(20, 120, this.globalService.pdfTxt[9])
      doc.text(20, 125, lines)
      doc.setFontSize(10)
      doc.setTextColor('#51bcda')
      /*doc.text(20, 225, this.feedbackFijoComun[0].text)
      doc.textWithLink(this.feedbackFijoComun[1].text, 20, 230, { url: this.feedbackFijoComun[1].link })
      doc.textWithLink(this.feedbackFijoVivienda[0].text, 20, 235, { url: this.feedbackFijoVivienda[0].link })
      doc.textWithLink(this.feedbackFijoVivienda[1].text, 20, 240, { url: this.feedbackFijoVivienda[1].link })
      doc.textWithLink(this.feedbackFijoVivienda[2].text, 20, 245, { url: this.feedbackFijoVivienda[2].link })
      doc.textWithLink(this.feedbackFijoVivienda[4].text, 20, 250, { url: this.feedbackFijoVivienda[4].link })
      doc.textWithLink(this.feedbackFijoVivienda[5].text, 20, 255, { url: this.feedbackFijoVivienda[5].link })
      doc.textWithLink(this.feedbackFijoVivienda[6].text, 20, 260, { url: this.feedbackFijoVivienda[6].link })*/
      // más info
      doc.setTextColor('#51bcda')
      doc.textWithLink(this.globalService.pdfTxt[21], 20, 260, { url: 'https://renhata.es/es/ciudadania/consejos-para-rehabilitar-tu-vivienda-accesibilidad' })
      // footer
      doc.setTextColor('#40694d')
      doc.setFontType('italic')
      doc.text(20, 270, this.globalService.footerPDF)
      doc.save(this.globalService.pdfTxt[10])
    } else {
      this.globalService.showAlertLogin = true;
    }
  }
}

