import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../global.service';
import * as jsPDF from 'jspdf';
import { FdbService } from 'app/fdb.service';
import { FauthService } from 'app/fauth.service';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-medioambiente',
  templateUrl: './medioambiente.component.html',
  styleUrls: ['./medioambiente.component.scss']
})
export class MedioambienteComponent implements OnInit {

  feedback = [];
  feedbackClean = [];
  feedbackPuntos = '';
  puntosTotal = 0;
  items = [];
  feedbackExtra = [];
  input = {};
  public bdCADataObservable: Observable<{}>;
  private user: Observable<User>;
  showRecomendaciones = false;

  constructor(public globalService: GlobalService, public fdb: FdbService, public fauth: FauthService, translate: TranslateService) {
    this.user = fauth.afAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.getFromFirestoreChanges();
        }
      }
    );
    translate.get('PM-ITEMS').subscribe((res) => {
      this.items = res;
    });
    translate.get('PM-FEEDEXTRA').subscribe((res) => {
      this.feedbackExtra = res;
      this.feedback[11] = this.feedbackExtra[0].text;
      this.feedback[12] = this.feedbackExtra[1].text;
      this.feedback[13] = this.feedbackExtra[2].text;
      this.feedback[14] = this.feedbackExtra[3].text;
      this.feedback[15] = this.feedbackExtra[4].text;
      this.feedback[16] = this.feedbackExtra[5].text;
      this.feedback[17] = this.feedbackExtra[6].text;
      this.feedback[18] = this.feedbackExtra[7].text;
      this.feedback[19] = this.feedbackExtra[8].text;
    });

    translate.onLangChange.subscribe((event) => {
      translate.get('PM-ITEMS').subscribe((res) => {
        this.items = res;
      });
      translate.get('PM-FEEDEXTRA').subscribe((res) => {
        this.feedbackExtra = res;
        this.feedback[11] = this.feedbackExtra[0].text;
        this.feedback[12] = this.feedbackExtra[1].text;
        this.feedback[13] = this.feedbackExtra[2].text;
        this.feedback[14] = this.feedbackExtra[3].text;
        this.feedback[15] = this.feedbackExtra[4].text;
        this.feedback[16] = this.feedbackExtra[5].text;
        this.feedback[17] = this.feedbackExtra[6].text;
        this.feedback[18] = this.feedbackExtra[7].text;
        this.feedback[19] = this.feedbackExtra[8].text;
      });
    });
  }

  ngOnInit() {
  }

  onSelectionChange(i, feed, resp) {
    this.feedback[i] = feed;
    this.cleanFeedback();
    this.input[i] = resp;
    if (this.fauth.userDetails != null) {
      this.fdb.saveData('pm', this.fauth.userDetails.uid, this.input);
    }
  }

  cleanFeedback() {
    this.feedbackClean = [];
    let count = 0;
    for (let i = 0; i < this.feedback.length; i++) {
      const element = this.feedback[i];
      if (element && count < 14) {
        this.feedbackClean[count] = element;
        count++;
      }
    }
  }

  getFromFirestoreChanges() {
    if (this.fauth.userDetails != null) {
      this.bdCADataObservable = this.fdb.getObservable('pm', this.fauth.userDetails.uid);
      this.bdCADataObservable.subscribe(input => {
        if (input != null) {
          this.input = input;
          this.updateInput();
        }
      });
    }
  }

  updateInput() {
    // tslint:disable-next-line:forin
    for (const k in this.input) {
      this.feedback[k] = this.items[k].opciones[this.input[k]].feed;
    }
    this.cleanFeedback();
  }

  generatePDF() {
    if (this.fauth.userDetails != null) {
      this.showRecomendaciones = true;
      // cabecera , imágenes
      const doc = new jsPDF({ lineHeight: 1.5 })
      doc.addImage(this.globalService.imgData.logos, 'PNG', 15, 15, 180, 33)
      doc.addImage(this.globalService.imgData.pm, 'JPEG', 20, 50, 60, 40)
      doc.link(15, 15, 180, 33, { url: 'https://bes-viviendaturistica.com' });
      doc.setFontSize(16)
      doc.setFontType('bold')
      doc.setTextColor('#40694d')
      // titulo sección
      doc.text(90, 60, this.globalService.pdfTxt[0])
      doc.text(90, 70, this.globalService.pdfTxt[6])
      doc.text(90, 80, this.globalService.pdfTxt[18])

      // const linkCE = this.feedbackClean[0] === this.items[6].opciones[1].feed;
      let feedbackText = ''
      for (let i = 0; i < this.feedbackClean.length; i++) {
        feedbackText += this.feedbackClean[i] + '\n'
      }

      const lines = doc.setFontSize(9).splitTextToSize(feedbackText, 180)
      // fecha
      const d = new Date();
      doc.text(90, 90, d.toLocaleDateString('es-ES'));
      // datos básicos
      doc.setFontSize(10)
      doc.text(20, 100, this.globalService.pdfTxt[7] + this.globalService.toStringAddress())
      doc.text(20, 105, this.globalService.pdfTxt[8] + this.globalService.year + '    ' + this.globalService.toStringReforma())
      doc.setFontType('normal')
      // lineas feedback
      doc.setFontSize(9)
      doc.text(20, 115, lines)
      /* doc.setFontSize(10)
       if (linkCE) {
         doc.setTextColor('#51bcda')
         doc.textWithLink(this.globalService.pdfTxt[15], 142, 125, { url: this.items[6].opciones[1].link })
       }*/
      // link
      doc.setFontSize(10)
      doc.setTextColor('#51bcda')
      doc.textWithLink(this.globalService.pdfTxt[21], 20, 260, { url: 'https://renhata.es/es/ciudadania/consejos-sostenibilidad-edificio' })
      // footer
      doc.setTextColor('#40694d')
      doc.setFontType('italic')
      doc.text(20, 270, this.globalService.footerPDF)

      doc.save(this.globalService.pdfTxt[14])
    } else {
      this.globalService.showAlertLogin = true;
    }
  }

}
