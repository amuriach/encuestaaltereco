import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../global.service';
import { FdbService } from 'app/fdb.service';
import { FauthService } from 'app/fauth.service';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { TranslateService } from '@ngx-translate/core';

export interface BasicData { email: string; cp: number; anyo: number; direccion: string; reforma: string }

@Component({
  selector: 'app-cuestionario-inicio',
  templateUrl: './cuestionario-inicio.component.html',
  styleUrls: ['./cuestionario-inicio.component.scss']
})

export class CuestionarioInicioComponent implements OnInit {

  private user: Observable<User>;
  public bdBasicDataObservable: Observable<BasicData>;

  year;
  address;
  cp;
  reforma;

  constructor(public globalService: GlobalService, public fdb: FdbService, public fauth: FauthService, translate: TranslateService) {
    this.globalService.cleanCuestionarioInicio();
    this.year = null;
    this.address = null;
    this.cp = null;
    this.reforma = null;
    this.user = fauth.afAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.getFromFirestoreChanges();
        } else {
          this.globalService.cleanCuestionarioInicio();
          this.year = null;
          this.address = null;
          this.cp = null;
          this.reforma = null;
          this.globalService.changeActiveSection('home');
        }
      }
    );
  }

  ngOnInit() { }

  onYearChange() {
    if (this.globalService.getYear() !== this.year) {
      this.globalService.setYear(this.year);
      this.saveInFirestoreChanges();
      console.log('CHANGE - Year');
    }
  }
  onCPChange() {
    if (this.globalService.getCP() !== this.cp) {
      this.globalService.setCP(this.cp);
      this.saveInFirestoreChanges();
      console.log('CHANGE - CP');
    }
  }
  onAddressChange() {
    if (this.globalService.getAddress() !== this.address) {
      this.globalService.setAddress(this.address);
      this.saveInFirestoreChanges();
      console.log('CHANGE - address');
    }

  }
  onSelectionReforma(r) {
    if (this.globalService.getReforma() !== r) {
      this.reforma = r;
      this.globalService.setReforma(r);
      this.saveInFirestoreChanges();
      console.log('CHANGE - Reforma');
    }
  }
  saveInFirestoreChanges() {
    if (this.fauth.userDetails != null) {
      this.fdb.saveBasicData(this.fauth.userDetails.uid, this.fauth.userDetails.email, this.cp, this.year, this.address, this.reforma);
    }
  }
  getFromFirestoreChanges() {
    if (this.fauth.userDetails != null) {
      this.fdb.getBasicData(this.fauth.userDetails.uid);
      this.fdb.bdBasicDataObservable.subscribe(basicD => { // converting oberv in array
        if (basicD != null) {
          this.year = basicD.anyo;
          this.cp = basicD.cp;
          this.address = basicD.direccion;
          this.reforma = basicD.reforma;
          this.onYearChange();
          this.onCPChange();
          this.onAddressChange();
          this.onSelectionReforma(this.reforma);
        }
      });
    }
  }
}
