import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuestionarioInicioComponent } from './cuestionario-inicio.component';

describe('CuestionarioInicioComponent', () => {
  let component: CuestionarioInicioComponent;
  let fixture: ComponentFixture<CuestionarioInicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuestionarioInicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuestionarioInicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
