import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';

import { ComponentsComponent } from './components.component';
import { CuestionarioInicioComponent } from './cuestionario-inicio/cuestionario-inicio.component';
import { ConfortTermicoComponent } from './confort-termico/confort-termico.component';
import { ConfortAcusticoComponent } from './confort-acustico/confort-acustico.component';
import { MedioambienteComponent } from './medioambiente/medioambiente.component';
import { EspaciosComponent } from './espacios/espacios.component';
import { AdaptadoComponent } from './adaptado/adaptado.component';
import { LandingComponent } from './landing/landing.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        NouisliderModule,
        JWBootstrapSwitchModule,
        TranslateModule
        ],
    declarations: [
        ComponentsComponent,
        CuestionarioInicioComponent,
        ConfortTermicoComponent,
        ConfortAcusticoComponent,
        MedioambienteComponent,
        EspaciosComponent,
        AdaptadoComponent,
        LandingComponent
    ],
    entryComponents: [],
    exports: [ ComponentsComponent ]
})
export class ComponentsModule { }
