import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../global.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})

export class LandingComponent implements OnInit {

  constructor(public globalService: GlobalService, public translate: TranslateService) { }

  ngOnInit() {}

}
