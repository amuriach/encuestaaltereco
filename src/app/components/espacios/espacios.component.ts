import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../global.service';
import * as jsPDF from 'jspdf';
import { FdbService } from 'app/fdb.service';
import { FauthService } from 'app/fauth.service';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-espacios',
  templateUrl: './espacios.component.html',
  styleUrls: ['./espacios.component.scss']
})
export class EspaciosComponent implements OnInit {

  multiplicador = 0;
  valoresTope = [0, 0, 36.5, 45.5, 53, 63.5, 69.5, 80.5, 86.5];
  feedback = [];
  feedbackYear = '';
  feedbackEspacio = '';
  feedbackSuelto = [];
  items = [];
  input = {};
  public bdCADataObservable: Observable<{}>;
  private user: Observable<User>;
  showRecomendaciones = false;

  constructor(public globalService: GlobalService, public fdb: FdbService, public fauth: FauthService, public translate: TranslateService) {
    if (this.globalService.getYear() > 2002 || this.globalService.getYear() === 0) {
      this.feedbackYear = '';
    } else {
      translate.get('CE-FEEDYEAR').subscribe((res) => {
        this.feedbackYear = res;
      });
    }
    this.user = fauth.afAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.getFromFirestoreChanges();
        }
      }
    );
    translate.get('CE-ITEMS').subscribe((res) => {
      this.items = res;
    });
    translate.get('CE-FEEDSUELTO').subscribe((res) => {
      this.feedbackSuelto = res;
    });
    translate.onLangChange.subscribe((event) => {
      if (this.globalService.getYear() > 2002 || this.globalService.getYear() === 0) {
        this.feedbackYear = '';
      } else {
        translate.get('CE-FEEDYEAR').subscribe((res) => {
          this.feedbackYear = res;
        });
      }
      translate.get('CE-ITEMS').subscribe((res) => {
        this.items = res;
      });
      translate.get('CE-FEEDSUELTO').subscribe((res) => {
        this.feedbackSuelto = res;
      });
    });

  }

  ngOnInit() {
  }

  onInputChange(i, resp) {
    this.input[i] = resp;
    if (this.fauth.userDetails != null) {
      this.fdb.saveData('ce', this.fauth.userDetails.uid, this.input);
    }
    const ocup = this.items[0].input;
    this.multiplicador = this.items[1].input * 0.7;
    if (ocup > 2) {
      if (this.multiplicador > this.valoresTope[ocup]) {
        this.translate.get('CE-FEEDESPACIO1').subscribe((res) => {
          this.feedbackEspacio = res;
        });
      } else {
        this.translate.get('CE-FEEDESPACIO2').subscribe((res) => {
          this.feedbackEspacio = res;
        });
      }
    }
    if (this.globalService.getYear() > 2002 || this.globalService.getYear() === 0) {
      this.feedbackYear = '';
    } else {
      this.translate.get('CE-FEEDESPACIO3').subscribe((res) => {
        this.feedbackEspacio = res;
      });
    }
  }

  onSelectionChange(i, feed, resp) {
    this.input[i] = resp;
    if (this.fauth.userDetails != null) {
      this.fdb.saveData('ce', this.fauth.userDetails.uid, this.input);
    }
    this.feedback[i] = feed;
  }

  getFromFirestoreChanges() {
    if (this.fauth.userDetails != null) {
      this.bdCADataObservable = this.fdb.getObservable('ce', this.fauth.userDetails.uid);
      this.bdCADataObservable.subscribe(input => {
        if (input != null) {
          this.input = input;
          this.updateInput();
        }
      });
    }
  }

  updateInput() {
    this.feedback = [];
    // tslint:disable-next-line:forin
    for (const k in this.input) {
      this.items[k].input = this.input[k];
      if (this.items[k].opciones[this.input[k]]) {
        if (this.items[k].opciones[this.input[k]].feed) {
          this.feedback[k] = this.items[k].opciones[this.input[k]].feed;
        }
      }
    }
    const ocup = this.items[0].input;
    this.multiplicador = this.items[1].input * 0.7;
    if (ocup > 2) {
      if (this.multiplicador > this.valoresTope[ocup]) {
        this.translate.get('CE-FEEDESPACIO1').subscribe((res) => {
          this.feedbackEspacio = res;
        });
      } else {
        this.translate.get('CE-FEEDESPACIO2').subscribe((res) => {
          this.feedbackEspacio = res;
        });
      }
    }
    if (this.globalService.getYear() > 2002 || this.globalService.getYear() === 0) {
      this.feedbackYear = '';
    } else {
      this.translate.get('CE-FEEDESPACIO3').subscribe((res) => {
        this.feedbackYear = res;
      });
    }
  }

  generatePDF() {
    if (this.fauth.userDetails != null) {
      this.showRecomendaciones = true;
      // cabecera , imágenes
      const doc = new jsPDF({ lineHeight: 1.5 })
      doc.addImage(this.globalService.imgData.logos, 'PNG', 15, 15, 180, 33)
      doc.addImage(this.globalService.imgData.ce, 'JPEG', 20, 50, 60, 40)
      doc.link(15, 15, 180, 33, { url: 'https://bes-viviendaturistica.com' });
      doc.setFontSize(16)
      doc.setFontType('bold')
      doc.setTextColor('#40694d')
      // titulo sección
      doc.text(90, 60, this.globalService.pdfTxt[0])
      doc.text(90, 70, this.globalService.pdfTxt[2])
      doc.text(90, 80, this.globalService.pdfTxt[1])

      const feedbackClean = this.globalService.cleanFeedback(this.feedback)

      let feedbackText = ''
      for (let i = 0; i < feedbackClean.length; i++) {
        feedbackText += feedbackClean[i] + '\n'
      }
      for (let i = 0; i < this.feedbackSuelto.length; i++) {
        feedbackText += this.feedbackSuelto[i] + '\n'
      }
      feedbackText += this.feedbackYear + '\n'

      const lines = doc.setFontSize(10).splitTextToSize(feedbackText, 170)
      const linesGlobal = doc.setFontSize(10).splitTextToSize(this.feedbackEspacio, 170)
      // fecha
      const d = new Date();
      doc.text(90, 90, d.toLocaleDateString('es-ES'));
      // datos básicos
      doc.text(20, 100, this.globalService.pdfTxt[7] + this.globalService.toStringAddress())
      doc.text(20, 105, this.globalService.pdfTxt[8] + this.globalService.year + '    ' + this.globalService.toStringReforma())
      // valoración global
      doc.setFontType('bold')
      doc.text(20, 120, this.globalService.pdfTxt[19])
      doc.setFontType('normal')
      doc.text(20, 125, linesGlobal)
      doc.setFontType('bold')
      doc.text(20, 140, this.globalService.pdfTxt[20])
      doc.setFontType('normal')
      // lineas feedback
      doc.text(20, 145, lines)
      // más info
      doc.setTextColor('#51bcda')
      doc.textWithLink(this.globalService.pdfTxt[21], 20, 260, { url: 'https://renhata.es/es/ciudadania/consejos-para-rehabilitar-tu-vivienda-funcionalidad' })
      // footer
      doc.setTextColor('#40694d')
      doc.setFontType('italic')
      doc.text(20, 270, this.globalService.footerPDF)
      doc.save(this.globalService.pdfTxt[11])
    } else {
      this.globalService.showAlertLogin = true;
    }
  }

}







