import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfortTermicoComponent } from './confort-termico.component';

describe('ConfortTermicoComponent', () => {
  let component: ConfortTermicoComponent;
  let fixture: ComponentFixture<ConfortTermicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfortTermicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfortTermicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
