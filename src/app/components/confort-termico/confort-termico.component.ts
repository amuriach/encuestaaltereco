import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../global.service';
import * as jsPDF from 'jspdf';
import { FdbService } from 'app/fdb.service';
import { FauthService } from 'app/fauth.service';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-confort-termico',
  templateUrl: './confort-termico.component.html',
  styleUrls: ['./confort-termico.component.scss']
})
export class ConfortTermicoComponent implements OnInit {

  year;
  feedback = [];
  feedbackClean = [];
  feedbackYear = [];
  feedbakPuntos = [];
  feedbakPuntosText = '';
  feedbakPuntosYearText = '';
  feedbackPuntosResult = []
  items = [];

  itemsSelected = [[]];
  input = {};
  public bdCADataObservable: Observable<{}>;
  private user: Observable<User>;
  showRecomendaciones = false;

  constructor(public globalService: GlobalService, public fdb: FdbService, public fauth: FauthService, public translate: TranslateService) {
    this.user = fauth.afAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.getFromFirestoreChanges();
        }
      }
    );
    translate.get('CT-ITEMS').subscribe((res) => {
      this.items = res;
    });
    translate.get('CT-FEEDPUNTOS').subscribe((res) => {
      this.feedbakPuntos = res;
    });
    translate.get('CT-FEEDYEAR').subscribe((res) => {
      this.feedbackYear = res;
    });
    translate.onLangChange.subscribe((event) => {
      this.translate.get('CT-ITEMS').subscribe((res) => {
        this.items = res;
      });
      this.translate.get('CT-FEEDPUNTOS').subscribe((res) => {
        this.feedbakPuntos = res;
      });
      this.translate.get('CT-FEEDYEAR').subscribe((res) => {
        this.feedbackYear = res;
      });
    });
  }

  ngOnInit() {
    if (this.globalService.getYear() > 2006) {
      this.feedback[15] = this.feedbackYear[2];
    } else if (this.globalService.getYear() > 1977) {
      this.feedback[15] = this.feedbackYear[1];
    } else {
      this.feedback[15] = this.feedbackYear[0];
    }
  }

  onSelectionChange(i, feed, puntos, resp) {
    this.input[i] = resp;
    if (this.fauth.userDetails != null) {
      this.fdb.saveData('ct', this.fauth.userDetails.uid, this.input);
    }
    this.feedback[i + 1] = feed;
    this.feedbackPuntosResult[i] = puntos;
    const totalPuntos = this.getSumPuntos()
    if (totalPuntos > 13) {
      this.feedbakPuntosText = this.feedbakPuntos[3];
    } else if (totalPuntos > 7) {
      this.feedbakPuntosText = this.feedbakPuntos[2];
    } else if (totalPuntos > 4) {
      this.feedbakPuntosText = this.feedbakPuntos[1];
    } else {
      this.feedbakPuntosText = this.feedbakPuntos[0];
    }

    if (this.globalService.getYear() > 2006) {
      this.feedbakPuntosYearText = this.feedbackYear[2];
    } else if (this.globalService.getYear() > 1977) {
      this.feedbakPuntosYearText = this.feedbackYear[1];
    } else {
      this.feedbakPuntosYearText = this.feedbackYear[0];
    }
    this.feedbackClean = this.globalService.cleanFeedback(this.feedback)
  }

  getSumPuntos() {
    let total = 0;
    for (let i = 0; i < this.feedbackPuntosResult.length; i++) {
      total += parseInt(this.feedbackPuntosResult[i], 10);
    }
    return total;
  }

  getFromFirestoreChanges() {
    if (this.fauth.userDetails != null) {
      this.bdCADataObservable = this.fdb.getObservable('ct', this.fauth.userDetails.uid);
      this.bdCADataObservable.subscribe(input => {
        if (input != null) {
          this.input = input;
          this.updateInput();
        }
      });
    }
  }

  updateInput() {
    this.feedback = [];
    // tslint:disable-next-line:forin
    for (const k in this.input) {
      this.feedback[k + 1] = this.items[k].opciones[this.input[k]].feed;
      this.feedbackPuntosResult[k] = this.items[k].opciones[this.input[k]].puntos;
    }
    const totalPuntos = this.getSumPuntos()
    if (totalPuntos > 13) {
      this.feedbakPuntosYearText = this.feedbakPuntos[3];
    } else if (totalPuntos > 7) {
      this.feedbakPuntosYearText = this.feedbakPuntos[2];
    } else if (totalPuntos > 4) {
      this.feedbakPuntosYearText = this.feedbakPuntos[1];
    } else {
      this.feedbakPuntosYearText = this.feedbakPuntos[0];
    }

    if (this.globalService.getYear() > 2006) {
      this.feedbakPuntosText = this.feedbackYear[2];
    } else if (this.globalService.getYear() > 1977) {
      this.feedbakPuntosText = this.feedbackYear[1];
    } else {
      this.feedbakPuntosText = this.feedbackYear[0];
    }
    this.feedbackClean = this.globalService.cleanFeedback(this.feedback)
  }

  generatePDF() {
    if (this.fauth.userDetails != null) {
      this.showRecomendaciones = true;
      // cabecera , imágenes
      const doc = new jsPDF({ lineHeight: 1.5 })
      doc.addImage(this.globalService.imgData.logos, 'PNG', 15, 15, 180, 33)
      doc.addImage(this.globalService.imgData.ct, 'JPEG', 20, 50, 60, 40)
      doc.link(15, 15, 180, 33, { url: 'https://bes-viviendaturistica.com' });
      doc.setFontSize(16)
      doc.setFontType('bold')
      doc.setTextColor('#40694d')
      // titulo sección
      doc.text(90, 60, this.globalService.pdfTxt[0])
      doc.text(90, 70, this.globalService.pdfTxt[4])
      doc.text(90, 80, this.globalService.pdfTxt[1])

      let feedbackText = ''
      for (let i = 0; i < this.feedbackClean.length; i++) {
        feedbackText += this.feedbackClean[i] + '\n'
      }
      const lines = doc.setFontSize(10).splitTextToSize(feedbackText, 170)
      const linesGlobal = doc.setFontSize(10).splitTextToSize(this.feedbakPuntosText + '\n' + this.feedbakPuntosYearText, 170)

      // fecha
      const d = new Date();
      doc.text(90, 90, d.toLocaleDateString('es-ES'));
      // datos básicos
      doc.text(20, 100, this.globalService.pdfTxt[7] + this.globalService.toStringAddress())
      doc.text(20, 105, this.globalService.pdfTxt[8] + this.globalService.year + '    ' + this.globalService.toStringReforma())
      doc.setFontType('normal')
      // valoración global
      doc.setFontType('bold')
      doc.text(20, 120, this.globalService.pdfTxt[19])
      doc.setFontType('normal')
      doc.text(20, 125, linesGlobal)
      doc.setFontType('bold')
      doc.text(20, 150, this.globalService.pdfTxt[20])
      doc.setFontType('normal')
      // lineas feedback
      doc.text(20, 155, lines)
      doc.setFontSize(10)
      // más info
      doc.setTextColor('#51bcda')
      doc.textWithLink(this.globalService.pdfTxt[21], 20, 260, { url: 'https://renhata.es/es/ciudadania/consejos-para-rehabilitar-tu-vivienda-generalidades' })
      // footer
      doc.setTextColor('#40694d')
      doc.setFontType('italic')
      doc.text(20, 270, this.globalService.footerPDF)
      doc.save(this.globalService.pdfTxt[13])
    } else {
      this.globalService.showAlertLogin = true;
    }
  }
}
