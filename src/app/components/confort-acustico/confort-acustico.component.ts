import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../global.service';
import * as jsPDF from 'jspdf';
import { FdbService } from 'app/fdb.service';
import { FauthService } from 'app/fauth.service';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-confort-acustico',
  templateUrl: './confort-acustico.component.html',
  styleUrls: ['./confort-acustico.component.scss']
})
export class ConfortAcusticoComponent implements OnInit {

  public bdCADataObservable: Observable<{}>;
  private user: Observable<User>;
  year;
  feedback = [];
  feedbackClean = [];
  puntosPregunta = [];
  feedExtra = [];
  feedbackYear = '';
  feedbackPuntos = '';
  puntosTotal = 0;
  input = {}; // {0: 0, 1: 0, 2: 0, 3: 0};
  items = []
  showRecomendaciones = false;

  constructor(public globalService: GlobalService, public fdb: FdbService, public fauth: FauthService, translate: TranslateService) {
    this.user = fauth.afAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.getFromFirestoreChanges();
        }
      }
    );
    translate.get('CA-ITEMS').subscribe((res) => {
      this.items = res;
    });
    translate.get('CA-FEEDEXTRA').subscribe((res) => {
      this.feedExtra = res;
    });

    translate.onLangChange.subscribe((event) => {
      translate.get('CA-ITEMS').subscribe((res) => {
        this.items = res;
      });
      translate.get('CA-FEEDEXTRA').subscribe((res) => {
        this.feedExtra = res;
      });
    });
  }

  ngOnInit() {
  }

  onSelectionChange(i, feed, p, resp) {
    //  this.itemsSelected[i][io] = true;
    this.feedback[i] = feed;
    this.puntosPregunta[i] = p;
    this.puntosTotal = 0;
    this.input[i] = resp;
    if (this.fauth.userDetails != null) {
      this.fdb.saveData('ca', this.fauth.userDetails.uid, this.input);
    }
    this.puntosPregunta.forEach(e => {
      this.puntosTotal = this.puntosTotal + e;
    });
    const puntos = 10 - this.puntosTotal / 22 * 10;
    if (puntos > 8) {
      this.feedbackPuntos = this.feedExtra[0];
    } else if (puntos >= 7) {
      this.feedbackPuntos = this.feedExtra[1];
    } else if (puntos >= 5) {
      this.feedbackPuntos = this.feedExtra[2];
    } else {
      this.feedbackPuntos = this.feedExtra[3];
    }
    this.feedbackClean = this.globalService.cleanFeedback(this.feedback)
    this.updateYearFeedback();
  }

  updateYearFeedback() {
    this.year = this.globalService.getYear();
    if (this.year !== 0) {
      if (this.year > 2008) {
        this.feedbackYear = this.feedExtra[4];

      } else if (this.year > 1988) {
        this.feedbackYear = this.feedExtra[5];

      } else {
        this.feedbackYear = this.feedExtra[6];
      }
    }
  }

  getFromFirestoreChanges() {
    if (this.fauth.userDetails != null) {
      this.bdCADataObservable = this.fdb.getObservable('ca', this.fauth.userDetails.uid);
      this.bdCADataObservable.subscribe(input => { // converting oberv in array
        if (input != null) {
          this.input = input;
          this.updateInput();
        }
      });
    }
  }

  updateInput() {
    this.feedback = [];
    // tslint:disable-next-line:forin
    for (const k in this.input) {
      this.feedback[k] = this.items[k].opciones[this.input[k]].feed;
      this.puntosPregunta[k] = this.items[k].opciones[this.input[k]].puntos;
    }

    this.puntosTotal = 0;
    this.puntosPregunta.forEach(e => {
      this.puntosTotal = this.puntosTotal + e;
    });
    const puntos = 10 - this.puntosTotal / 22 * 10;
    if (puntos > 8) {
      this.feedbackPuntos = this.feedExtra[0];
    } else if (puntos >= 7) {
      this.feedbackPuntos = this.feedExtra[1];
    } else if (puntos >= 5) {
      this.feedbackPuntos = this.feedExtra[2];
    } else {
      this.feedbackPuntos = this.feedExtra[3];
    }
    this.feedbackClean = this.globalService.cleanFeedback(this.feedback)
    this.updateYearFeedback();

  }

  generatePDF() {
    if (this.fauth.userDetails != null) {
      this.showRecomendaciones = true;
      // cabecera , imágenes
      const doc = new jsPDF({ lineHeight: 1.5 })
      doc.addImage(this.globalService.imgData.logos, 'PNG', 15, 15, 180, 33)
      doc.addImage(this.globalService.imgData.ca, 'JPEG', 20, 50, 60, 40)
      doc.link(15, 15, 180, 33, { url: 'https://bes-viviendaturistica.com' });
      doc.setFontSize(16)
      doc.setFontType('bold')
      doc.setTextColor('#40694d')
      // titulo sección
      doc.text(90, 60, this.globalService.pdfTxt[0])
      doc.text(90, 70, this.globalService.pdfTxt[5])
      doc.text(90, 80, this.globalService.pdfTxt[1])
      let feedbackText = ''
      for (let i = 0; i < this.feedbackClean.length; i++) {
        feedbackText += this.feedbackClean[i] + '\n'
      }
      feedbackText = feedbackText

      const lines = doc.setFontSize(10).splitTextToSize(feedbackText, 170)
      const linesGlobal = doc.setFontSize(10).splitTextToSize(this.feedbackYear + '\n' + this.feedbackPuntos, 170)
      // fecha
      const d = new Date();
      doc.text(90, 90, d.toLocaleDateString('es-ES'));
      // datos básicos
      doc.text(20, 100, this.globalService.pdfTxt[7] + this.globalService.toStringAddress())
      doc.text(20, 105, this.globalService.pdfTxt[8] + this.globalService.year + '    ' + this.globalService.toStringReforma())
      doc.setFontType('normal')
      // valoración global
      doc.setFontType('bold')
      doc.text(20, 120, this.globalService.pdfTxt[19])
      doc.setFontType('normal')
      doc.text(20, 125, linesGlobal)
      doc.setFontType('bold')
      doc.text(20, 150, this.globalService.pdfTxt[20])
      doc.setFontType('normal')
      // lineas feedback
      doc.text(20, 155, lines)
      doc.setFontSize(10)
      // más info
      doc.setTextColor('#51bcda')
      doc.textWithLink(this.globalService.pdfTxt[21], 20, 260, { url: 'https://renhata.es/es/ciudadania/consejos-para-rehabilitar-tu-vivienda-confort-acustico' })
      // footer
      doc.setTextColor('#40694d')
      doc.setFontType('italic')
      doc.text(20, 270, this.globalService.footerPDF)
      doc.save(this.globalService.pdfTxt[12])
    } else {
      this.globalService.showAlertLogin = true;
    }
  }
}
