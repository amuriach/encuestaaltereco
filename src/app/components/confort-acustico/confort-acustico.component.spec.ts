import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfortAcusticoComponent } from './confort-acustico.component';

describe('ConfortAcusticoComponent', () => {
  let component: ConfortAcusticoComponent;
  let fixture: ComponentFixture<ConfortAcusticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfortAcusticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfortAcusticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
