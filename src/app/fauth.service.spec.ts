import { TestBed, inject } from '@angular/core/testing';

import { FauthService } from './fauth.service';

describe('FauthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FauthService]
    });
  });

  it('should be created', inject([FauthService], (service: FauthService) => {
    expect(service).toBeTruthy();
  }));
});
