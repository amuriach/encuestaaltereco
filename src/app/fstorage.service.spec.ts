import { TestBed, inject } from '@angular/core/testing';

import { FstorageService } from './fstorage.service';

describe('FstorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FstorageService]
    });
  });

  it('should be created', inject([FstorageService], (service: FstorageService) => {
    expect(service).toBeTruthy();
  }));
});
