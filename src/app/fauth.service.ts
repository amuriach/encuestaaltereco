import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth, User } from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class FauthService {

  private user: Observable<User>;
  public userDetails: User = null;
  public errorLogin;
  public errorRegister;
  public errors = [];

  constructor(public afAuth: AngularFireAuth, translate: TranslateService) {
    this.errorLogin = '';
    this.errorRegister = '';
    this.user = afAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.userDetails = user;
          console.log(this.userDetails);
        } else {
          this.userDetails = null;
        }
      }
    );
    translate.get('AUTH-ERRORS').subscribe((res) => {
      this.errors = res;
    });
  }

  login(e, p) {
    this.afAuth.auth.signInWithEmailAndPassword(e, p).catch(error => {
      const errorCode = error.code;
      if (errorCode === 'auth/weak-password') {
        this.errorLogin = this.errors[3]
      } else if (errorCode === 'auth/user-disabled') {
        this.errorLogin = this.errors[7]
      } else if (errorCode === 'auth/invalid-email') {
        this.errorLogin = this.errors[4]
      } else if (errorCode === 'auth/user-not-found') {
        this.errorLogin = this.errors[8]
      } else if (errorCode === 'auth/wrong-password') {
        this.errorLogin = this.errors[9]
      } else {
        this.errorLogin = error.message
      }
    });
  }

  register(e, p) {
    this.afAuth.auth.createUserWithEmailAndPassword(e, p).catch(error => {
      const errorCode = error.code;
      if (errorCode === 'auth/weak-password') {
        this.errorRegister = this.errors[3]
      } else if (errorCode === 'auth/email-already-in-use') {
        this.errorRegister = this.errors[2]
      } else if (errorCode === 'auth/invalid-email') {
        this.errorRegister = this.errors[4]
      } else if (errorCode === 'auth/operation-not-allowed') {
        this.errorRegister = this.errors[5]
      } else {
        this.errorRegister = error.message
      }
    });
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  resetPwd(e) {
    this.afAuth.auth.sendPasswordResetEmail(e).then(e => {
      // Email sent.
      this.errorLogin = this.errors[0]
    }).catch(error => {
      // An error happened.
      const errorCode = error.code;
      if (errorCode === 'auth/invalid-email') {
        this.errorRegister = this.errors[4]
      } else {
        if (error != null) {
          this.errorLogin = this.errors[1]
        }
      }
    });
  }
}
