import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../global.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    test: Date = new Date();

    constructor(public globalService: GlobalService, public translate: TranslateService) { }

    ngOnInit() {}
}
