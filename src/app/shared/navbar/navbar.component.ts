import { Component, OnInit, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { FauthService } from 'app/fauth.service';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { GlobalService } from '../../global.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    private toggleButton: any;
    private sidebarVisible: boolean;

    public email;
    public pwd;
    public emailReg;
    public pwdReg;
    public pwdRepReg;
    public acepto = false;
    public aceptoReg = false;
    private user: Observable<User>;

    constructor(public location: Location, private element: ElementRef, public fauth: FauthService, public globalService: GlobalService, public translate: TranslateService) {
        this.sidebarVisible = false;
        this.user = fauth.afAuth.authState;
        this.user.subscribe(
            (user) => {
                if (user) {
                    $('#loginModal').modal('hide');
                    $('#registerModal').modal('hide');
                }
            }
        );
    }

    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    }
    login() {
        if (this.acepto) {
            if (this.email != null && this.pwd != null && this.email !== '' && this.pwd !== '') {
                this.fauth.login(this.email, this.pwd);
            } else {
                this.fauth.errorLogin = 'Por favor, introduce email y password.'
            }
        } else {
            this.fauth.errorLogin = 'Tienes que aceptar la política de Protección de Datos'
        }
    }
    register() {
        if (this.aceptoReg) {
            if (this.emailReg != null && this.pwdReg != null && this.pwdRepReg != null && this.emailReg !== '' && this.pwdReg !== '' && this.pwdRepReg !== '') {
                if (this.pwdRepReg === this.pwdReg) {
                    this.fauth.register(this.emailReg, this.pwdReg);
                } else {
                    this.fauth.errorRegister = 'Revisa tu contraseña. No coinciden.'
                }
            } else {
                this.fauth.errorRegister = 'Por favor, introduce email, password y repite password.'
            }
        } else {
            this.fauth.errorRegister = 'Tienes que aceptar la política de Protección de Datos'
        }
    }
    logout() {
        this.fauth.logout();
    }
    resetPassword() {
        if (this.email !== '' && this.email != null) {
            this.fauth.resetPwd(this.email);
        } else {
            this.fauth.errorLogin = 'Indica tu email para recuperar el password.'
        }
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const html = document.getElementsByTagName('html')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        html.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];
        // console.log(html);
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };

}
