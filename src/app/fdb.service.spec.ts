import { TestBed, inject } from '@angular/core/testing';

import { FdbService } from './fdb.service';

describe('FdbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FdbService]
    });
  });

  it('should be created', inject([FdbService], (service: FdbService) => {
    expect(service).toBeTruthy();
  }));
});
